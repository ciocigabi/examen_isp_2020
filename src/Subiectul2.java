import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class Subiectul2 extends JFrame{

    JTextArea tArea;
    JButton adauga;

   Subiectul2(){

        setTitle("Gabriel Cioci-S2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,350);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=150;int height = 30;

        tArea = new JTextArea();
        tArea.setBounds(10,50,width, 7*height);

        adauga = new JButton("Adauga numar");
        adauga.setBounds(10,10,width, height);
        adauga.addActionListener(new HandleAdd());

        add(tArea);
        add(adauga);
    }

    class HandleAdd implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int randomNumber = (int)(Math.random()*100);
            Subiectul2.this.tArea.append(Integer.toString(randomNumber)+"\n");
        }
    }


    public static void main(String[] args) {
        new Subiectul2();
    }
}