public class Base {
    float n;
    void f(int g) {
    }
}

class L extends Base {
    private long t;

    @Override
    public void f(int g) {
        super.f(g);
    }
}

class X {
    public void i(L i){
    }
}

class A {
    M m = new M();
    public void metA() {};
}

class B {
    M n;
    public void metB() {};
}

class M {
    L l;
}